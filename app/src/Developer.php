<?php

namespace App;

class Developer extends Human implements PrimateInterface
{
    public string $direction;
    public string $grade = 'junior';

    use OfficePlancton;

    /**
     * @return string
     */
    public function eat(): string
    {
        return 'метод eat';
    }

    /**
     * @return string
     */
    public function drink(): string
    {
        return $this->drinkCoffee();
    }

    /**
     * @return string
     */
    public function goToWork(): string
    {
        return 'метод ходить на работу';
    }
}