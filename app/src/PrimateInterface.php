<?php

namespace App;

interface PrimateInterface
{
    public function eat(): string;

    public function drink(): string;
}