<?php

namespace App;

abstract class Human
{
    abstract public function goToWork(): string;
}