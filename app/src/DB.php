<?php

namespace App;

use mysqli;

class DB
{
    private static $instance;
    public mysqli $db;
    private $connection;

    private function __construct()
    {
        $this->db = new mysqli('localhost', 'user', 'password', 'database');
    }

    public static function getInstance(): DB
    {
        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }

}

DB::getInstance()->getConnection();